﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shop.Application.Repositories;
using Shop.Infrastructure.Repository;

namespace Shop.Infrastructure;

public static class DependecyInjection {

    public static IServiceCollection ImplementPersistence(this IServiceCollection services, IConfiguration configuration) {

        services.AddDbContext<AppDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"),
            b => b.MigrationsAssembly("Shop.Infrastructure")), ServiceLifetime.Transient);

        services.AddScoped<IAppDbContext>(provider => provider.GetService<AppDbContext>());
        services.AddScoped<IProductRepository, ProductRepository>();
        services.AddScoped<ICategoryRepository, CategoryRepository>();
        
        

        return services;
    }
}