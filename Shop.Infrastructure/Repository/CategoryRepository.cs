﻿using Microsoft.EntityFrameworkCore;
using Shop.Application.Repositories;
using Shop.Domain.Entities;

namespace Shop.Infrastructure.Repository;

public class CategoryRepository: ICategoryRepository
{
    private readonly AppDbContext _context;

    public CategoryRepository(AppDbContext context)
    {
        _context = context;
    }

    public async Task<Category> AddCategory(Category category)
    {
        var existingCategory  = await _context.Categories.FirstOrDefaultAsync(x=>x == category);
        if (existingCategory != null)
        {
            return existingCategory;
        }
        await _context.Categories.AddAsync(category);
        await _context.SaveChangesAsync();

        return category;
    }

    public async Task<IEnumerable<Category>> GetAll()
    {
        return await _context.Categories.ToListAsync();
    }

    public async Task<bool> Delete(Guid categoryId)
    {
       var category =  _context.Categories.Find(categoryId);
       
       if (category == null) return false;
       
       _context.Categories.Remove(category);
       return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Category> GetById(Guid categoryId)
    {
        return await _context.Categories.FindAsync(categoryId);
    }
}