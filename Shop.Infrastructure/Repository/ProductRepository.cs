﻿using Microsoft.EntityFrameworkCore;
using Shop.Application.DTOs;
using Shop.Application.Repositories;
using Shop.Domain.Entities;

namespace Shop.Infrastructure.Repository;

public class ProductRepository:IProductRepository
{
    private readonly AppDbContext _context;

    public ProductRepository(AppDbContext context)
    {
        _context = context;
    }

    public async Task<bool> AddProduct(Product product, Guid categoryId)
    {
        product.Category = await _context.Categories.FindAsync(categoryId);
        await _context.Products.AddAsync(product);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<IEnumerable<Product>> Get(string name, string desc, double price, string categoryName, string color, int size)
    {
        var query = _context.Products.AsQueryable();

        if (!string.IsNullOrEmpty(name))
        {
            query = query.Where(p => p.Name.Contains(name));
        }

        if (!string.IsNullOrEmpty(desc))
        {
            query = query.Where(p => p.Desc.Contains(desc));
        }

        if (price > 0)
        {
            query = query.Where(p => p.Price == price);
        }

        if (!string.IsNullOrEmpty(categoryName))
        {
            query = query.Where(p => p.Category.Name.Contains(categoryName));
        }

        if (!string.IsNullOrEmpty(color))
        {
            query = query.Where(p => p.Category.Color.Contains(color));
        }

        if (size > 0)
        {
            query = query.Where(p => p.Category.Size == size);
        }

        return await query.ToListAsync();
    }

    public async Task<Product> GetById(Guid id)
    {
       return await _context.Products.Include(x=>x.Category).FirstOrDefaultAsync(x => x.Id == id);
    }
}