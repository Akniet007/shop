﻿namespace Shop.Domain.Entities;

public class Product
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string Name { get; set; }
    public string Desc { get; set; }
    public double Price { get; set; }
    public Category Category { get; set; }
}