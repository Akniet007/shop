﻿namespace Shop.Domain.Entities;

public class Category
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string Color { get; set; }
    public string Name { get; set; }
    public double Weight { get; set; }
    public int Size { get; set; }
    public IEnumerable<Product> Products { get; set; }
    
    
    public bool Equals(Category other)
    {
        if (other == null)
            return false;

        return Color == other.Color &&
               Name == other.Name &&
               Weight.Equals(other.Weight) &&
               Size == other.Size &&
               Products.SequenceEqual(other.Products);
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as Category);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Color, Weight, Size, Products , Name);
    }

    public static bool operator ==(Category left, Category right)
    {
        if (ReferenceEquals(left, right))
            return true;

        if (left is null || right is null)
            return false;

        return left.Equals(right);
    }

    public static bool operator !=(Category left, Category right)
    {
        return !(left == right);
    }
}