﻿using AutoMapper;
using Shop.Application.DTOs;
using Shop.Application.Interfaces;
using Shop.Application.Repositories;
using Shop.Domain.Entities;

namespace Shop.Application.Services;

public class ProductService:IProductService
{
    private readonly IProductRepository _productRepository;
    private readonly ICategoryRepository _categoryRepository;
    private readonly IMapper _mapper;
    
    public ProductService(IProductRepository productRepository, IMapper mapper, ICategoryRepository categoryRepository)
    {
        _productRepository = productRepository;
        _mapper = mapper;
        _categoryRepository = categoryRepository;
    }

    public async Task<bool> AddProduct(ProductCreateDto createDto)
    {
        
        var product = _mapper.Map<Product>(createDto);
        return await _productRepository.AddProduct(product,createDto.CategoryId);     
    }

    public async Task<IEnumerable<ProductDto>> Get(string name, string desc, double price, string categoryName, string color, int size)
    {
       var products =  await _productRepository.Get(name, desc, price, categoryName, color, size);
       return _mapper.Map<IEnumerable<ProductDto>>(products);
    }

    public async Task<ProductFullInfoDto> Get(Guid id)
    {
       return _mapper.Map<ProductFullInfoDto>( await _productRepository.GetById(id));
    }
}