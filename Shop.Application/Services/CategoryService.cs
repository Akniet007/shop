﻿using AutoMapper;
using Shop.Application.DTOs;
using Shop.Application.Interfaces;
using Shop.Application.Repositories;
using Shop.Domain.Entities;

namespace Shop.Application.Services;

public class CategoryService:ICategoryService
{
    private readonly ICategoryRepository _categoryRepository;
    private readonly IMapper _mapper;
    
    public CategoryService(ICategoryRepository categoryRepository, IMapper mapper)
    {
        _categoryRepository = categoryRepository;
        _mapper = mapper;
    }

    public async Task<CategoryDto> Add(CateryCreateDto createDto)
    {
        return  _mapper.Map<CategoryDto>(await _categoryRepository.AddCategory(_mapper.Map<Category>(createDto)));
    }

    public async Task<IEnumerable<CategoryDto>> GetAll()
    {
        return _mapper.Map<IEnumerable<CategoryDto>>( await _categoryRepository.GetAll());
    }

    public async Task<bool> Delete(Guid categoryId)
    {
       return await _categoryRepository.Delete(categoryId);
    }
}