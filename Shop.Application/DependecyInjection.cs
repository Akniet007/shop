﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shop.Application.Helper;
using Shop.Application.Interfaces;
using Shop.Application.Services;

namespace Shop.Application;

public static class DependecyInjection
{
    public static IServiceCollection ImplementService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAutoMapper(typeof(Mapping).Assembly);
        services.AddScoped<IProductService, ProductService>();
        services.AddScoped<ICategoryService, CategoryService>();
        
        
        return services;
    }
}