﻿using Shop.Application.DTOs;

namespace Shop.Application.Interfaces;

public interface ICategoryService
{
    Task<CategoryDto> Add(CateryCreateDto createDto);
    Task<IEnumerable<CategoryDto>> GetAll();
    Task<bool> Delete(Guid categoryId);
}