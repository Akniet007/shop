﻿using Shop.Application.DTOs;
using Shop.Domain.Entities;

namespace Shop.Application.Interfaces;

public interface IProductService
{
    Task<bool> AddProduct(ProductCreateDto createDto);
    Task<IEnumerable<ProductDto>> Get(string name, string desc, double price, string categoryName, string color, int size);
    Task<ProductFullInfoDto> Get(Guid id);
}