﻿using AutoMapper;
using Shop.Application.DTOs;
using Shop.Domain.Entities;

namespace Shop.Application.Helper;

public class Mapping:Profile
{
    public Mapping()
    {
        CreateMap<ProductCreateDto, Product>();
        CreateMap<CateryCreateDto , Category>();
        CreateMap<Category , CategoryDto>();
        CreateMap<Product, ProductFullInfoDto>()
            .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.Category.Color))
            .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
            .ForMember(dest => dest.Size, opt => opt.MapFrom(src => src.Category.Size))
            .ForMember(dest => dest.Weight, opt => opt.MapFrom(src => src.Category.Weight));
    }
}