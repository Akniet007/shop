﻿namespace Shop.Application.DTOs;

public class ProductDto
{
    public string Name { get; set; }
    public string Desc { get; set; }
    public double Price { get; set; }
    
}