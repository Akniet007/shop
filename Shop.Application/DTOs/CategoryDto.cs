﻿namespace Shop.Application.DTOs;

public class CategoryDto
{
    public Guid Id { get; set; }
    public string Color { get; set; }
    public double Weight { get; set; }
    public int Size { get; set; }
    public string Name { get; set; }
}