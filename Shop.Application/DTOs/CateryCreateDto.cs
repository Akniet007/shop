﻿namespace Shop.Application.DTOs;

public class CateryCreateDto
{
    public string Color { get; set; }
    public string Name { get; set; }
    public double Weight { get; set; }
    public int Size { get; set; }
}