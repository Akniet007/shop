﻿namespace Shop.Application.DTOs;

public class ProductFullInfoDto
{
    public string Name { get; set; }
    public string Desc { get; set; }
    public double Price { get; set; }
    //category
    public string Color { get; set; }
    public string CategoryName { get; set; }
    public double Weight { get; set; }
    public int Size { get; set; }
}