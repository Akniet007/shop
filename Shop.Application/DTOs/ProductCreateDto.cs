﻿namespace Shop.Application.DTOs;

public class ProductCreateDto
{
    public string Name { get; set; }
    public string Desc { get; set; }
    public double Price { get; set; }
    public Guid CategoryId { get; set; }
}