﻿using Shop.Domain.Entities;

namespace Shop.Application.Repositories;

public interface ICategoryRepository
{
    Task<Category> AddCategory(Category category);
    Task<IEnumerable<Category>> GetAll();
    Task<bool> Delete(Guid categoryId);
    Task<Category> GetById(Guid createDtoCategoryId);
}