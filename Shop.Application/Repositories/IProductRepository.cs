﻿using Shop.Application.DTOs;
using Shop.Domain.Entities;

namespace Shop.Application.Repositories;

public interface IProductRepository
{
    Task<bool> AddProduct(Product createDto , Guid categoryId);
    Task<IEnumerable<Product>> Get(string name, string desc, double price, string categoryName, string color, int size);
    Task<Product> GetById(Guid id);
}