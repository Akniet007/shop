﻿using Microsoft.AspNetCore.Mvc;
using Shop.Application.DTOs;
using Shop.Application.Interfaces;

namespace Shop.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CategoryController:ControllerBase
{
    private readonly ICategoryService _categoryService;

    public CategoryController(ICategoryService categoryService)
    {
        _categoryService = categoryService;
    }


    [HttpPost("add")]
    public async Task<IActionResult> AddCategory(CateryCreateDto createDto)
    {
       return  Ok(await _categoryService.Add(createDto));
    }

    [HttpGet("all")]
    public async Task<IActionResult> GetAllCategory()
    {
        return Ok(await _categoryService.GetAll());
    }

    [HttpDelete("delete")]
    public async Task<IActionResult> DeleteCategory([FromQuery] Guid categoryId)
    {
       if(await _categoryService.Delete(categoryId))
       {
           return Ok();
       }

       return BadRequest();
    }
}