﻿using Microsoft.AspNetCore.Mvc;
using Shop.Application.DTOs;
using Shop.Application.Interfaces;

namespace Shop.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProductController: ControllerBase
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }

    [HttpPost("add/product")]
    public async Task<IActionResult> AddProduct(ProductCreateDto createDto )
    {
        
        return Ok(await _productService.AddProduct(createDto));
    }

    [HttpGet("products")]
    public async Task<IActionResult> GetProducts([FromQuery] string name, [FromQuery] string desc,
        [FromQuery] double price, [FromQuery] string categoryName , [FromQuery] string color ,[FromQuery] int size)
    { 
        var res = await  _productService.Get(name , desc , price, categoryName , color , size);
        return Ok(res);
    }

    [HttpGet("product/{id}")]
    public async Task<IActionResult> GetProduct(Guid id)
    {
       var product =  await _productService.Get(id);
       if (product == null) return NotFound();
       return Ok(product);
    }
    
}